Annotation
==========

**Use Diamond to annotate the predicted genes**

Source the configuration file::

	source conf_cluster.conf

Run Blast alignment using Diamond::

	diamond blastp --query $GENE_PRED/prodigal.idba.$PRJ_NAME.faa \
	--db $DB/sub_refSeq \
	-o $GENE_PRED/diamond.prodigal.idba.$PRJ_NAME.m8 \
	--sensitive


Have a look to the results::

	cd $GENE_PRED
	ll
	less diamond.prodigal.idba.$PRJ_NAME.m8
